import React, { Component } from "react";
import "./style.css";
import kesehatan from "./kesehatan";
import { Route, NavLink, HashRouter } from "react-router-dom";
class peserta extends Component {
  render() {
    return (
      <div>
        <h2>Data Peserta</h2>

        <form>
            <label>
                    <tr>
                        <td> Nama:
                            <input type="text" name="name" /> 
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Nik:
                            <input type="numeric" name="nik" />
                        </td>
                    </tr>
                    <tr>
                        <td> Email:
                            <input type="text" name="email" /> 
                        </td>
                    </tr>
                    <tr>
                        <td>
                            No Hp:
                            <input type="text" name="hp" />
                        </td>
                    </tr>
                    <tr>
                        <td> Alamat:
                            <input type="text" name="alamat" /> 
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Tempat Lahir:
                            <input type="text" name="tl" />
                        </td>
                    </tr>
                    <tr>
                        <td> Tanggal Lahir:
                            <input type="text" name="ttl" /> 
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Sex:
                            <input type="text" name="sex" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Pendapatan (Yearly):
                            <input type="text" name="fee" />
                        </td>
                    </tr>
                    <li><a href="">Cancel</a></li> 
                    <li><NavLink to="/kesehatan">Next</NavLink></li>
                    <Route path="/kesehatan" component={kesehatan}/>     
            </label>
        </form> 
    </div>
    );
  }
}
 

export default peserta;